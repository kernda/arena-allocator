#include <stdio.h> //TODO: remove this import
#include <sys/mman.h>
#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>

#define MAX_MEM_PAGE_COUNT 500

size_t page_size;

typedef struct MemoryPage_ {
    void* mem_start;
    void* next_free_mem_slot_start;
    size_t allocated_chunk_count;
    size_t size;
    bool is_freed;
} MemoryPage;

uint8_t* get_mem_page_end(MemoryPage* page) {
    return (uint8_t*)page->mem_start + page->size;
}

static struct Heap {
    MemoryPage* pages; 
    size_t page_count; 
} heap;

void* allocate_system_page() {
    return mmap(NULL, page_size, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
}

static __attribute__((constructor)) void heap_init() {
    page_size = getpagesize();

    void* mem_page = allocate_system_page();
    
    heap.pages = mem_page; //pointers to memmory pages will be stored at the begining of the first page
    
    MemoryPage first_mem_page = {
        .mem_start = (MemoryPage*)mem_page + MAX_MEM_PAGE_COUNT,
        .next_free_mem_slot_start = (MemoryPage*)mem_page + MAX_MEM_PAGE_COUNT,
        .allocated_chunk_count = 1,
        .size = page_size,
        .is_freed = false
    };

    heap.pages[0] = first_mem_page;
    heap.page_count = 1;
}


//public API
void* alloc(size_t size) {
    //TODO: make sure that size is pow(2) aligned

    void* memory = NULL;
    int first_freed_page_idx = -1;

    for(int i=0; i<heap.page_count; ++i) {
        uint8_t* next_slot = (uint8_t*)heap.pages[i].next_free_mem_slot_start;
        uint8_t* page_end = get_mem_page_end(&heap.pages[i]);

        //there is enough space in one of the already allocated memory pages
        if(next_slot + size < page_end) {
            memory = heap.pages[i].next_free_mem_slot_start;
            heap.pages[i].next_free_mem_slot_start = (uint8_t*)heap.pages[i].next_free_mem_slot_start + size;
            heap.pages[i].allocated_chunk_count++;

            return memory;
        }

        //find first freed page in our list in case we don't find a suitable one in this loop
        if(heap.pages[i].is_freed) {
            first_freed_page_idx = i;
        }
    }
   
    if(first_freed_page_idx > 0) {
        heap.pages[first_freed_page_idx].next_free_mem_slot_start = (uint8_t*)heap.pages[first_freed_page_idx].mem_start + size;
        heap.pages[first_freed_page_idx].allocated_chunk_count = 0;
        heap.pages[first_freed_page_idx].size = page_size;
        heap.pages[first_freed_page_idx].is_freed = false;

        memory = heap.pages[first_freed_page_idx].next_free_mem_slot_start; 
    }
    else if(heap.page_count < MAX_MEM_PAGE_COUNT) {
        //no memory page in the list has been freed
        //let's allocate a new one if the max number of pages hasn't been reached
        void* new_mem_page = allocate_system_page();

        MemoryPage mem_page = {
            .mem_start = new_mem_page,
            .next_free_mem_slot_start = new_mem_page,
            .allocated_chunk_count = 0,
            .size = page_size,
            .is_freed = false
        };

        heap.pages[heap.page_count] = mem_page;
        heap.page_count++;

        memory = mem_page.next_free_mem_slot_start; 
    }

    return memory;
}

// void free_mem(void* ptr) {
// }

int main() {
    
    
    return 0;
}